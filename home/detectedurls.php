<?php
function exception_handler($exception) {
	echo "<pre>";
  	echo "Uncaught exception: " , $exception->getMessage(), "\n";
	echo "</pre>";
	echo "</body>";
	echo "</html>";
}

set_exception_handler('exception_handler'); 

if(!@include("vars.php")) throw new Exception("Failed to include 'vars.php'"); 

    $cockpit_admin_name = "cockpit-admin-web-app";
    $cockpit_admin_url = "";
    $cockpit_name = "cockpit-web-app";
    $cockpit_url = "";
    $xsa_admin_name = "xsa-admin";
    $xsa_admin_url = "";
    $space_enable_name = "di-space-enablement-ui";
    $space_enable_url = "";


$output = `/hana/shared/HXE/xs/bin/xs api https://$fqdn:$xsa_api_port --skip-ssl-validation ; /hana/shared/HXE/xs/bin/xs login -u $xsa_admin_un -p $xsa_admin_pw -o $org -s SAP ; /hana/shared/HXE/xs/bin/xs app $cockpit_admin_name --urls`;
//$output = "/hana/shared/HXE/xs/bin/xs api https://$fqdn:$xsa_api_port --skip-ssl-validation ; /hana/shared/HXE/xs/bin/xs login -u $xsa_admin_un -p $xsa_admin_pw -o $org -s SAP ; /hana/shared/HXE/xs/bin/xs app $cockpit_admin_name --urls";

//print($output);

$outparts = explode("\n", $output);

$outsize = sizeof($outparts);

// echo "outsize: " . $outsize . "\n";

$cockpit_admin_url = $outparts[$outsize-3];
echo "Cockpit Admin URL: <a href=\"" . $cockpit_admin_url . "\">" . $cockpit_admin_url . "</a><br />\n";

$output = `/hana/shared/HXE/xs/bin/xs app $cockpit_name --urls`;
$outparts = explode("\n", $output);
$outsize = sizeof($outparts);
$cockpit_url = $outparts[$outsize-3];
echo "Cockpit URL: <a href=\"" . $cockpit_url . "\">" . $cockpit_url . "</a><br />\n";

$output = `/hana/shared/HXE/xs/bin/xs app $xsa_admin_name --urls`;
$outparts = explode("\n", $output);
$outsize = sizeof($outparts);
$xsa_admin_url = $outparts[$outsize-3];
echo "XSA Admin URL: <a href=\"" . $xsa_admin_url . "\">" . $xsa_admin_url . "</a><br />\n";

$output = `/hana/shared/HXE/xs/bin/xs app $space_enable_name --urls`;
$outparts = explode("\n", $output);
$outsize = sizeof($outparts);
$space_enable_url = $outparts[$outsize-3];
echo "Space Enablement URL: <a href=\"" . $space_enable_url . "\">" . $space_enable_url . "</a><br />\n";


?>
