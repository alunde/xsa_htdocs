<html>
<head>
<?php 
	require 'vars.php';
	$ip = file_get_contents('https://api.ipify.org');
?>
<title><?php print($company); ?> XS-A</title>
</head>

<body style="font-family: Tahoma, Geneva, sans-serif">

<center>
<h1><?php print($company); ?> Server Post Launch Setup</h1>
</center>
<h3><font color="red">Warning:</font> <?php print($ip); ?> In order for XS-A things to work you <font color="red">MUST</font> override your local hostname resolution!</h3>
<ul>
	<li>
		<h3>In Windows:</h3>
		<ol>
		<li>Open Notepad in <font color="red">Run as administrator</font> mode.</li>
		<li>File -> Open C:\Windows\System32\drivers\etc\ File name: <strong>hosts</strong>  File Filter: All Files (*.*)</li>
		<li>Add the following line to the end of the file.<br /><pre><?php print($ip); ?>	hxe2.sfphcp.com</pre></li>
		<li>File -> Save</li>
		</ol>

	</li>
	<li>
		<h3>On Mac:</h3>
		<ol>
		<li><font color="red">Confiugure you Mac so that you have root privileges.</font>  <a href="https://support.apple.com/en-us/HT204012">Follow this.</a></li>
		<li>In the terminal use the nano editor to edit /etc/<strong>hosts</strong> <a href="http://osxdaily.com/2012/08/07/edit-<strong>hosts</strong>-file-mac-os-x/">Details here.</a></li>
		<li>Add the following line to the end of the file.<br /><pre><?php print($ip); ?>	hxe2.sfphcp.com</pre></li>
		<li>Control+O followed by ENTER/RETURN then Control+X to exit out of nano</li>
		</ol>

	</li>
</ul>
	<h3>Now you should be able to use any of the links on these pages.</h3>
	<h3><a href="/">Return to the main page.</a></h3>
</body>
</html>

