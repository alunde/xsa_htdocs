<html>
<head>
<?php 
	require 'vars.php';
	$ip = file_get_contents('https://api.ipify.org');
?>
<title>HTTP(s) Test</title>
</head>

<body style="font-family: Tahoma, Geneva, sans-serif">

<center>
<h1>HTTP(s) Test</h1>
<h1><?php print($_SERVER['HTTP_HOST']); ?></h1>
<h1><?php print("SSL=" . $_SERVER['HTTPS']); ?></h1>
</center>
<ul>
	<li><a href="http://<?php print($fqdn); ?>/home/httphttpstest.php">http://<?php print($fqdn); ?>/home/httphttpstest.php</a></li>
	<li><a href="https://<?php print($fqdn); ?>/home/httphttpstest.php">https://<?php print($fqdn); ?>/home/httphttpstest.php</a></li>
	<li><a href="http://<?php print($ip); ?>/home/httphttpstest.php">http://<?php print($ip); ?>/home/httphttpstest.php</a></li>
	<li><a href="https://<?php print($ip); ?>/home/httphttpstest.php">https://<?php print($ip); ?>/home/httphttpstest.php</a></li>

</ul>
	<h3><a href="/">Return to the main page.</a></h3>
</body>
</html>

