<?php
	function exception_handler($exception) {
		echo "<pre>";
  		echo "Uncaught exception: " , $exception->getMessage(), "\n";
		echo "</pre>";
		echo "</body>";
		echo "</html>";
	}

	set_exception_handler('exception_handler'); 

	if(!@include("vars.php")) throw new Exception("Failed to include 'vars.php'"); 

$output = `$xs_cli api $xsa_api_url --skip-ssl-validation ; $xs_cli login -u $xsa_admin_un -p $xsa_admin_pw -o $org -s SAP ; $xs_cli app $cockpit_admin_name --urls`;
//$output = "$xs_cli api $xsa_api_url --skip-ssl-validation ; $xs_cli login -u $xsa_admin_un -p $xsa_admin_pw -o $org -s SAP ; $xs_cli app $cockpit_admin_name --urls";

$outparts = explode("\n", $output);
$outsize = sizeof($outparts);
$cockpit_admin_url = $outparts[$outsize-3];

$output = `$xs_cli app $cockpit_name --urls`;
$outparts = explode("\n", $output);
$outsize = sizeof($outparts);
$cockpit_url = $outparts[$outsize-3];

$output = `$xs_cli app $xsa_admin_name --urls`;
$outparts = explode("\n", $output);
$outsize = sizeof($outparts);
$xsa_admin_url = $outparts[$outsize-3];

$output = `$xs_cli app $space_enable_name --urls`;
$outparts = explode("\n", $output);
$outsize = sizeof($outparts);
$space_enable_url = $outparts[$outsize-3];


?>
<html>
<head>
<title><?php print($company); ?> XS-A</title>
</head>

<body style="font-family: Tahoma, Geneva, sans-serif">
<center>
<h1><?php print($company); ?> HXE2 SP01 XS-Advanced Server</h1>
</center>
<h3>XS-Advanced Editors:</h3>
<!-- <pre><?php print($output); ?></pre> -->
<ul>
	<li>XS-Advanced Web_IDE: <a id="xsa_webide" href="<?php print($xsa_webide_url); ?>" target="webide"><?php print($xsa_webide_url); ?></a> un: <?php print($xsa_un); ?> pw: <?php print($xsa_pw_ob); ?></li>
	<li>Cockpit Admin URL: <a id="cockpit_admin" href="<?php print($cockpit_admin_url); ?>" target="cockpit_admin"><?php print($cockpit_admin_url); ?></a> un: <?php print($xsa_admin_un); ?> pw: <?php print($xsa_admin_pw_ob); ?></li>
	<li>Cockpit URL: <a id="cockpit" href="<?php print($cockpit_url); ?>" target="cockpit"><?php print($cockpit_url); ?></a> un: <?php print($xsa_admin_un); ?> pw: <?php print($xsa_admin_pw_ob); ?></li>
	<li>XSA Admin URL: <a id="xsa_admin_url" href="<?php print($xsa_admin_url); ?>" target="xsa_admin"><?php print($xsa_admin_url); ?></a> un: <?php print($xsa_admin_un); ?> pw: <?php print($xsa_admin_pw_ob); ?></li>
	<li>Space Enablement URL: <a id="space_enable_url" href="<?php print($space_enable_url); ?>" target="space_enable"><?php print($space_enable_url); ?></a> un: <?php print($xsa_admin_un); ?> pw: <?php print($xsa_admin_pw_ob); ?></li>
</ul>

<!--
<h3>GitLab and Jenkins:</h3>
<a href="https://<?php print($fqdn); ?>:8060/" target="gitlab">GitLab Server</a><br />
<a href="https://<?php print($fqdn); ?>:8090/" target="jenkins">Jenkins Server</a><br />
<br />
-->

<h3>HANA XS-Advanced Client command line tool:</h3>
<ul>
	<li><a href="/files/xsa_cmd_line_app/xs.onpremise.runtime.client_darwinintel64.zip" target="files">OSX64 XS Command Line Tool ZIP</a></li>
	<li><a href="/files/xsa_cmd_line_app/xs.onpremise.runtime.client_linuxx86_64.zip" target="files">Linux64 XS Command Line Tool ZIP</a></li>
	<li><a href="/files/xsa_cmd_line_app/xs.onpremise.runtime.client_ntamd64.zip" target="files">Windows64 XS Command Line Tool ZIP</a></li>
	<li><a href="/files/xsa_cmd_line_app/xs_javascript-1.13.3-bundle.zip" target="files">Javascript Bundle ZIP</a></li>
</ul>

<!-- <h4>NOTE: Once you click on one of the following links, your browser will able to access the links above due to HSTS caching.</h4> -->
<ul>
	<li>XS-Advanced Info: <a id="xsa_info_id" href="<?php print($xsa_info_url); ?>" target="info"><?php print($xsa_info_url); ?></a> JSON Output</li>
</ul>
<h3>Example XS-Advanced Client commands:</h3>
<pre>
<strong>xs api <?php print($xsa_api_url); ?> --cacert /home/ec2-user/xsa_pem/default.root.crt.pem</strong>

  API endpoint: <?php print($xsa_api_url); ?> <br />
  SSL trust: The authenticity of host '<?php print($xsa_api_url); ?>' is established by the '/home/ec2-user/xsa_pem/default.root.crt.pem' certificate.

<strong>xs login -u XSA_ADMIN</strong>

<strong>xs target -s SAP</strong>

<strong>xs app webide --urls</strong> 

<strong>xs apps</strong>

<strong>xs mtas</strong>

<p>Once this is working, you can watch the xs-a apps with this script.<p>
Just change into the SAP space with <strong>xs target -s SAP</strong> and then run this script.<br />
<strong>while sleep 10; do clear; xs a; done</strong> <br />
Ctrl-C to break.<br />
<br />
Note that the XS-A system takes a while to start up once HANA has started and this is a good way to verify it's up.<br />
</pre>

<h3>HANA XS-Advanced Resources:</h3>

<ul>
	<li><a href="https://help.sap.com/viewer/42668af650f84f9384a3337bcd373692/2.0.02/en-US/23e4560b5d2e43be95330a9bc4cbacc8.html" target="docs">SAP HANA2 SP01 Release Notes</a></li>

	<li><a href="https://help.sap.com/viewer/4505d0bdaf4948449b7f7379d24d0f0d/2.0.02/en-US/" target="docs">SAP HANA Developer Guide XS-Advanced</a></li>

        <li><a href="https://help.sap.com/viewer/e8e6c8142e60469bb401de5fdb6f7c00/2.0.02/en-US/509dce3fd6f44a839e9c0ae83dc62d04.html" target="docs">SAP HANA XS-A Modeling Guide</a></li>

        <li><a href="https://help.sap.com/viewer/09b6623836854766b682356393c6c416/2.0.02/en-US/0b1eb07d74ec4f91947ff4cc4f557429.html" target="docs">SAP HANA Core Data Services Reference</a></li>

        <li><a href="https://help.sap.com/viewer/4fe29514fd584807ac9f2a04f6754767/2.0.02/en-US/209eaa85751910149a30f95c936075be.html" target="docs">SAP HANA SQL and SYS Views Reference</a></li>


        <li><a href="https://help.sap.com/viewer/4505d0bdaf4948449b7f7379d24d0f0d/2.0.02/en-US/addd59069e6f444ca6ccc064d131feec.html" target="docs">SAP HANA XS-A CLI Reference</a></li>


	<li><a href="http://scn.sap.com/community/developer-center/hana/blog/2016/03/28/developing-with-xs-advanced-a-tinyworld-tutorial" target="docs">XS-A Tinyworld Tutorial</a> Follow all the steps in the tutoral.</li>
	<li><a href="https://www.youtube.com/playlist?list=PLkzo92owKnVwL3AWaWVbFVrfErKkMY02a" target="docs">HANA Academy XS-A Videos</a></li>
</ul>

<h3>XS-Advanced self-contained examples:</h3>

<ul>
	<li><a href="https://alunde@bitbucket.org/alunde/mta_iot.git" target="bitbucket">Simple CDS and ODATA (NodeJS+XSJS) example</a></li>
	<li><a href="https://alunde@bitbucket.org/alunde/mta_ta.git" target="bitbucket">Simple Text Analytics example</a></li>
	<li><a href="https://alunde@bitbucket.org/alunde/mta_tspa.git" target="bitbucket">Simple Time Series Predictive Analysis example</a></li>
</ul>


<h3>Open SAP Courses:</h3>

<ul>
	<li><a href="https://open.sap.com/courses" target="opensap">All Courses</a></li>
	<li><a href="https://open.sap.com/courses/hana5" target="opensap">Software Development on SAP HANA (Update Q4/2016) Course</a></li>
	<li><a href="https://open.sap.com/courses/hssh1" target="opensap">openSAP SHINE Course</a></li>
</ul>

<br />
<br />

<h3>XS-Classic Editors: (Soon to be deprecated.  Use above if possible.)</h3>
<ul>
	<li>Login: <a id="login_id" href="<?php print($login_url); ?>" target="login"><?php print($login_url); ?></a> un: <?php print($db_un); ?> pw: <?php print($db_pw); ?> </li>
	<li>Cockpit: Replaced with HANA Cockpit which is not installed on this system. Find the installer in /install/HANACOCKPIT</li>
	<li>Workbench: <a id="workbench_id" href="<?php print($workbench_url); ?>" target="workbench"><?php print($workbench_url); ?></a></li>
	<li>Editor: <a id="editor_id" href="<?php print($editor_url); ?>" target="editor"><?php print($editor_url); ?></a></li>
	<li>Catalog: <a id="catalog_id" href="<?php print($catalog_url); ?>" target="catalog"><?php print($catalog_url); ?></a></li>
	<li>Security: <a id="security_id" href="<?php print($security_url); ?>" target="security"><?php print($security_url); ?></a> Needed to create new XS-Advanced users. un: <?php print($db_un); ?> pw: <?php print($db_pw); ?></li>
	<li>XS Admin: <a id="xsadmin_id" href="<?php print($xsadmin_url); ?>" target="xsadmin"><?php print($xsadmin_url); ?></a></li>
</ul>

<br />
<br />
	<h3><a href="/">Return to the main page.</a></h3>

</body>
</html>

