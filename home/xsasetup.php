<html>
<head>
<?php 
	require 'vars.php';
?>
<title><?php print($company); ?> XS-A</title>
</head>

<body style="font-family: Tahoma, Geneva, sans-serif">

<center>
<h1><?php print($company); ?> HANA XS-Advanced Server Post Launch Setup</h1>
</center>
<h3>Post-launch Setup: <strong>Warning: Things are changing with SP01 so these instructions may not be exact!</strong></h3>
<ul>
	<li>Login: <a id="login_id" href="<?php print($login_url); ?>" target="login"><?php print($login_url); ?></a> un: <?php print($db_un); ?> pw: <?php print($db_pw); ?> </li>
</ul>
<ul>
<ol>
<li>SSH into <strong><?php print($fqdn); ?></strong> and perform the following as root with <strong>sudo /bin/bash</strong>.</li>
	<ul>
	<li>Edit the <strong>vi /etc/hosts</strong> file and place the server's FQDN (<strong><?php print($fqdn); ?></strong>) at the end of the 127.0.0.1 line.</li>
	<li>Verify with ping that the <strong>ping <?php print($fqdn); ?></strong> resolves to 127.0.0.1 locally.</li>
	<li>Use <strong>/hana/shared/XSA/hdblcm/hdblcm</strong> to rename the server to <strong><?php print($fqdn); ?></strong>.  10 | rename_system</li>
	<li>Restart the services as <strong> su - xsaadm</strong> with <strong>./HDB stop ; ./HDB start</strong></li>
	</ul>
<li>Connect to the server with HANA Studio.</li>
	<ul>
	<li>Configure xscontroller.ini -> communication -> default_domain to <strong><?php print($fqdn); ?></strong>.</li>
	<li>--OR--</li>
	<li>WAS!: <strong>hdbsql -u <?php print($db_un); ?> "alter system alter configuration('xscontroller.ini','SYSTEM') SET ('communication','default_domain') = '<?php print($fqdn); ?>' with reconfigure"</strong></li>
	<li>Enable the Script Server</li>
	<li>WAS!: <strong>hdbsql -u <?php print($db_un); ?> "alter system alter configuration('daemon.ini', 'SYSTEM') SET ('scriptserver','instances') = '1' with reconfigure"</strong></li>
	<li>NOW!: <strong>ALTER DATABASE XSA ADD 'scriptserver' AT LOCATION '<?php print($fqdn); ?>:30004'</strong> On the TENNANT SYSTEMDB!</li>
	<li>(Optional) Generate a new license for the server and install it.</li>
	</ul>
<li>Become the xsaadm user with <strong>su - xsaadm</strong> if you aren't already.</li>
	<ul>
	<li>Restart the services with <strong>./HDB stop ; ./HDB start</strong></li>
	</ul>
<li>Drop back to the ec2-user with <strong>exit</strong> to root and then <strong>exit</strong> again.</li>
	<ul>
	<li>Grab the regenerated cert with <strong>sudo cp /hana/shared/XSA/xs/controller_data/controller/ssl-pub/router/default.root.crt.pem /home/ec2-user/xsa_pem</strong></li>
	<li>Reset the API Endpoint <strong>xs api https://<?php print($fqdn); ?>:30030 --cacert /home/ec2-user/xsa_pem/default.root.crt.pem</strong></li>
	<li>Attach the XS client to the local API and login with XSA_ADMIN:(common password) <strong>xs login -u XSA_ADMIN</strong></li>
	<li>Target the SAP space <strong>xs target -s SAP</strong></li>
	<li>Rename the org with <strong>xs rename-org sfphcp <?php print($org); ?></strong></li>
	<li>WAS!: Fix the hrtt-service with <strong>xs set-env hrtt-service WEBSOCKET_ORIGIN '[ "<?php print($fqdn); ?>" ]'</strong></li>
	<li>WAS!: Run the <strong>./restage_sap_all</strong> script found in /home/ec2-user</li>
	<li>Use the di-space-enablement-ui tool as XSA_ADMIN to Redeploy the di-builder in the <?php print($space); ?> space. <strong><a href="<?php print($xsa_space_enablement_url); ?>" target="space_enable"><?php print($xsa_space_enablement_url); ?></a></strong></li>
	<li>Logout as the XSA_ADMIN.</li>
	</ul>
<li>Test by importing Git example</li>
	<ul>
	<li>Use the webide as XSA_USER. <strong><a href="<?php print($xsa_webide_url); ?>" target="webide"><?php print($xsa_webide_url); ?></a></strong> (might have to use icognito or different browser)</li>
	<li>Git -> Clone Repository <strong>https://bitbucket.org/alunde/mta_iot.git</strong></li>
	<li>Set the space to <strong><?php print($space); ?></strong> in the project settings.</li>
	<li>Build the db<strong></strong></li>
	<li>Build the js<strong></strong></li>
	<li>Check the DB artifacts in the webide by Adding a new HDI-Containter from the space where you have performed an HDI module build. <strong><a href="<?php print($xsa_webide_url); ?>" target="hrtt"><?php print($xsa_webide_url); ?></a></strong> (might have to use icognito or different browser)</li>
	</ul>
</ol>
</body>
</html>

